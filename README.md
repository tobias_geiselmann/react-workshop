# React Workshop App

This is a demo application for the React Workshop. You can find the documentation [here](https://arborsys.atlassian.net/l/c/X6mhhdMn).

## Getting started

Run the following commands to install all dependencies and run the application:

```bash
npm install
npm run start
```