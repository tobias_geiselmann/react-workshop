import React from 'react'
import { useHistory } from 'react-router'

export function HistoryDemo() {
  const history = useHistory()

  const goBack = () => {
    history.goBack()
  }

  const goForward = () => {
    history.goForward()
  }

  return (
    <>
      <button onClick={goBack}>Go back</button>
      <button onClick={goForward}>Go forward</button>
    </>
  )
}
