import React from 'react'
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom'

export function RouterDemo() {
  return (
    <Router>
      <Switch>
        <Route path="/link-1">
          <h2>Page 1</h2>
        </Route>
        <Route path="/link-2">
          <h2>Page 2</h2>
        </Route>
      </Switch>
      <Link to="/link-1">Link 1</Link>
      <Link to="/link-2">Link 2</Link>
    </Router>
  )
}
