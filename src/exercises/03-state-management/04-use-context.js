import React, { useContext, useState } from 'react'

const CountContext = React.createContext()

function Text() {
  const [count] = useContext(CountContext)
  return <p>You clicked {count} times</p>
}

function Button() {
  const [count, setCount] = useContext(CountContext)
  return (
    <button type="submit" onClick={() => setCount(count + 1)}>
      Click me
    </button>
  )
}

export function ComposedContextCounter() {
  const [count, setCount] = useState(0)

  return (
    <div>
      <CountContext.Provider value={[count, setCount]}>
        <Text />
        <Button />
      </CountContext.Provider>
    </div>
  )
}
