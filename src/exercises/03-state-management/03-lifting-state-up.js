import React, { useState } from 'react'

function Text({ count }) {
  return <p>You clicked {count} times</p>
}

function Button({ count, setCount }) {
  return (
    <button type="submit" onClick={() => setCount(count + 1)}>
      Click me
    </button>
  )
}

export function ComposedCounter() {
  const [count, setCount] = useState(0)

  return (
    <div>
      <Text count={count} />
      <Button count={count} setCount={setCount} />
    </div>
  )
}
