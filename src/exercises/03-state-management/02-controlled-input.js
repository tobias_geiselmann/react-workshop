import React, { useState } from 'react'

export function ControlledInput({ initialName = '' }) {
  const [name, setName] = useState(initialName)

  function handleChange(event) {
    setName(event.target.value)
  }
  return (
    <>
      <form>
        <label>
          Name:
          <input value={name} onChange={handleChange} type="text" name="name" />
        </label>
      </form>
      {name ? <strong>Hello {name}</strong> : 'Please type your name'}
      <br />
      <input type="submit" value="Submit" />
    </>
  )
}
