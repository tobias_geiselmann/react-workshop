import React from 'react'

function Welcome(props) {
  return <h1>Hello, {props.name}</h1>
}

export function SimpleFunctionComponent() {
  return <Welcome name="Frank" />
}
