import React from 'react'
import { Welcome } from './02-composing-components'

export function ReactFragment() {
  return (
    <React.Fragment>
      <Welcome name="Sara" />
      <Welcome name="Peter" />
      <Welcome name="Frank" />
    </React.Fragment>
  )
}
