import React from 'react'

export function Welcome(props) {
  return <h1>Hello, {props.name}</h1>
}

export function ComposingComponents() {
  return (
    <div>
      <Welcome name="Sara" />
      <Welcome name="Peter" />
      <Welcome name="Frank" />
    </div>
  )
}
