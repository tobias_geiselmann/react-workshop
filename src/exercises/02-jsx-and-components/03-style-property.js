import React from 'react'

const myStyle = {
  color: 'grey',
  fontSize: '24px',
}

function Welcome(props) {
  return <h1 style={myStyle}>Hello, {props.name}</h1>
}

export function StyledWelcome() {
  return <Welcome name="Frank" />
}
