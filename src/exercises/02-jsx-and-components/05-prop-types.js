import PropTypes from 'prop-types';
import React from 'react';

function Welcome(props) {
  return <h1>Hello, {props.name}</h1>;
}

Welcome.propTypes = {
  name: PropTypes.string,
};

export function PropTypesExample() {
  return <Welcome name={2} />;
}
