import React, { useState } from 'react';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    console.log(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>Something went wrong.</h1>;
    }

    return this.props.children;
  }
}

function Bug() {
  const [content, setContent] = useState([1, 2, 3]);
  return (
    <div>
      {content.map((item) => {
        return <div>{item}</div>;
      })}
      <button
        onClick={() => {
          setContent(undefined);
        }}
      >
        Produce error
      </button>
    </div>
  );
}

export function BugWithErrorBoundary() {
  return (
    <ErrorBoundary>
      <Bug />
    </ErrorBoundary>
  );
}
