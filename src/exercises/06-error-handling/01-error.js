import React, { useState } from 'react';

export function Bug() {
  const [content, setContent] = useState([1, 2, 3]);
  return (
    <div>
      {content.forEach((item) => {
        <div>{item}</div>;
      })}
      <button
        onClick={() => {
          setContent(undefined);
        }}
      >
        Produce error
      </button>
    </div>
  );
}
