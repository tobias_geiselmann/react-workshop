import React, { useEffect, useState, useCallback } from 'react'

export function CallbackUser() {
  const [userID, setUserID] = useState(1)
  const [user, setUser] = useState()

  // TODO: move the fetch call outside of useEffect and see what happens

  const runFetch = useCallback(() => {
    fetch(`https://reqres.in/api/users/${userID}`)
      .then(res => res.json())
      .then(data => {
        console.log(data)
        setUser(data.data)
      })
  }, [userID])

  useEffect(() => {
    runFetch()
  }, [runFetch])

  const fetchNextUser = () => {
    setUserID(userID + 1)
  }

  return (
    <>
      {user && <div>Hello, {user.first_name}</div>}
      <button type="submit" onClick={fetchNextUser}>
        Fetch next user
      </button>
    </>
  )
}
