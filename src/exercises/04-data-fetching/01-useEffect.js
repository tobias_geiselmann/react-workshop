import React, { useEffect, useState } from 'react'

export function User() {
  const [userID, setUserID] = useState(1)
  const [user, setUser] = useState()

  useEffect(() => {
    fetch(`https://reqres.in/api/users/${userID}`)
      .then(res => res.json())
      .then(data => {
        console.log(data)
        setUser(data.data)
      })
  }, [userID])

  const fetchNextUser = () => {
    setUserID(userID + 1)
  }

  return (
    <>
      {user && <div>Hello, {user.first_name}</div>}
      <button type="submit" onClick={fetchNextUser}>
        Fetch next user
      </button>
    </>
  )
}
