import React, { useState, useEffect, useLayoutEffect } from 'react'

export const BlinkyRender = () => {
  const [value, setValue] = useState(0)

  useEffect(() => {
    if (value === 0) {
      setValue(10 + Math.random() * 200)
    }
  }, [value])

  console.log('render', value)

  return (
    <>
      <h2>Blinking Render</h2>
      <div onClick={() => setValue(0)}>value: {value}</div>
    </>
  )
}

export const NonBlinkyRender = () => {
  const [value, setValue] = useState(0)

  useLayoutEffect(() => {
    if (value === 0) {
      setValue(10 + Math.random() * 200)
    }
  }, [value])

  console.log('render', value)

  return (
    <>
      <h2>Non-blinking Render</h2>
      <div onClick={() => setValue(0)}>value: {value}</div>
    </>
  )
}
