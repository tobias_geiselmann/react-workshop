import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const Navigation = () => {
  const [openMenuItems, setOpenMenuItems] = useState(new Set());

  const toggleMenuItem = (id) => {
    const newSet = new Set(openMenuItems);
    if (openMenuItems.has(id)) {
      newSet.delete(id);
      setOpenMenuItems(newSet);
    } else {
      newSet.add(id);
      setOpenMenuItems(newSet);
    }
  };

  return (
    <div className="flex flex-col flex-grow border-r border-gray-200 pt-5 pb-4 bg-white overflow-y-auto">
      <div className="flex items-center flex-shrink-0 px-4 prose prose">
        <h2>React Workshop</h2>
      </div>
      <div className="mt-5 flex-grow flex flex-col">
        <nav className="flex-1 px-2 space-y-1 bg-white" aria-label="Sidebar">
          <div className="space-y-1">
            {/* Current: "bg-gray-100 text-gray-900", Default: "bg-white text-gray-600 hover:bg-gray-50 hover:text-gray-900" */}
            <button
              type="button"
              className="bg-white text-gray-600 hover:bg-gray-50 hover:text-gray-900 group w-full flex items-center pr-2 py-2 text-sm font-medium rounded-md focus:outline-none focus:ring-2 focus:ring-indigo-500"
              aria-controls="sub-menu-1"
              aria-expanded="false"
              onClick={() => toggleMenuItem(1)}
            >
              {/* Expanded: "text-gray-400 rotate-90", Collapsed: "text-gray-300" */}
              <svg className="text-gray-300 mr-2 h-5 w-5 transform group-hover:text-gray-400 transition-colors ease-in-out duration-150" viewBox="0 0 20 20" aria-hidden="true">
                <path d="M6 6L14 10L6 14V6Z" fill="currentColor" />
              </svg>
              Exercise 1
            </button>
            {/* Expandable link section, show/hide based on state. */}
            {openMenuItems.has(1) && (
              <div className="space-y-1" id="sub-menu-1">
                <Link
                  to="01-raw-react-apis/01-create-html-element.html"
                  className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50"
                >
                  Create HTML element
                </Link>
                <Link
                  to="01-raw-react-apis/02-create-react-element.html"
                  className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50"
                >
                  React.createElement
                </Link>
              </div>
            )}
          </div>
          <div className="space-y-1">
            {/* Current: "bg-gray-100 text-gray-900", Default: "bg-white text-gray-600 hover:bg-gray-50 hover:text-gray-900" */}
            <button
              type="button"
              className="bg-white text-gray-600 hover:bg-gray-50 hover:text-gray-900 group w-full flex items-center pr-2 py-2 text-sm font-medium rounded-md focus:outline-none focus:ring-2 focus:ring-indigo-500"
              aria-controls="sub-menu-1"
              aria-expanded="false"
              onClick={() => toggleMenuItem(2)}
            >
              {/* Expanded: "text-gray-400 rotate-90", Collapsed: "text-gray-300" */}
              <svg className="text-gray-300 mr-2 h-5 w-5 transform group-hover:text-gray-400 transition-colors ease-in-out duration-150" viewBox="0 0 20 20" aria-hidden="true">
                <path d="M6 6L14 10L6 14V6Z" fill="currentColor" />
              </svg>
              Exercise 2
            </button>
            {/* Expandable link section, show/hide based on state. */}
            {openMenuItems.has(2) && (
              <div className="space-y-1" id="sub-menu-1">
                <Link to="/2/1" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  Simple Function Component
                </Link>
                <Link to="/2/2" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  Composed Component
                </Link>
                <Link to="/2/3" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  Style property
                </Link>
                <Link to="/2/4" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  React.Fragment
                </Link>
              </div>
            )}
          </div>
          <div className="space-y-1">
            {/* Current: "bg-gray-100 text-gray-900", Default: "bg-white text-gray-600 hover:bg-gray-50 hover:text-gray-900" */}
            <button
              type="button"
              className="bg-white text-gray-600 hover:bg-gray-50 hover:text-gray-900 group w-full flex items-center pr-2 py-2 text-sm font-medium rounded-md focus:outline-none focus:ring-2 focus:ring-indigo-500"
              aria-controls="sub-menu-2"
              aria-expanded="false"
              onClick={() => toggleMenuItem(3)}
            >
              {/* Expanded: "text-gray-400 rotate-90", Collapsed: "text-gray-300" */}
              <svg className="text-gray-300 mr-2 h-5 w-5 transform group-hover:text-gray-400 transition-colors ease-in-out duration-150" viewBox="0 0 20 20" aria-hidden="true">
                <path d="M6 6L14 10L6 14V6Z" fill="currentColor" />
              </svg>
              Exercise 3
            </button>
            {/* Expandable link section, show/hide based on state. */}
            {openMenuItems.has(3) && (
              <div className="space-y-1" id="sub-menu-2">
                <Link to="/3/1" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  Simple useState
                </Link>
                <Link to="/3/2" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  Controlled Input
                </Link>
                <Link to="/3/3" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  Lifting state up
                </Link>
                <Link to="/3/4" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  useContext
                </Link>
              </div>
            )}
          </div>
          <div className="space-y-1">
            {/* Current: "bg-gray-100 text-gray-900", Default: "bg-white text-gray-600 hover:bg-gray-50 hover:text-gray-900" */}
            <button
              type="button"
              className="bg-white text-gray-600 hover:bg-gray-50 hover:text-gray-900 group w-full flex items-center pr-2 py-2 text-sm font-medium rounded-md focus:outline-none focus:ring-2 focus:ring-indigo-500"
              aria-controls="sub-menu-3"
              aria-expanded="false"
              onClick={() => toggleMenuItem(4)}
            >
              {/* Expanded: "text-gray-400 rotate-90", Collapsed: "text-gray-300" */}
              <svg className="text-gray-300 mr-2 h-5 w-5 transform group-hover:text-gray-400 transition-colors ease-in-out duration-150" viewBox="0 0 20 20" aria-hidden="true">
                <path d="M6 6L14 10L6 14V6Z" fill="currentColor" />
              </svg>
              Exercise 4
            </button>
            {openMenuItems.has(4) && (
              <div className="space-y-1" id="sub-menu-3">
                <Link to="/4/1" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  useEffect
                </Link>
                <Link to="/4/2" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  useCallback
                </Link>
                <Link to="/4/3" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  useLayoutEffect
                </Link>
              </div>
            )}
          </div>
          <div className="space-y-1">
            {/* Current: "bg-gray-100 text-gray-900", Default: "bg-white text-gray-600 hover:bg-gray-50 hover:text-gray-900" */}
            <button
              type="button"
              className="bg-white text-gray-600 hover:bg-gray-50 hover:text-gray-900 group w-full flex items-center pr-2 py-2 text-sm font-medium rounded-md focus:outline-none focus:ring-2 focus:ring-indigo-500"
              aria-controls="sub-menu-4"
              aria-expanded="false"
              onClick={() => toggleMenuItem(5)}
            >
              {/* Expanded: "text-gray-400 rotate-90", Collapsed: "text-gray-300" */}
              <svg className="text-gray-300 mr-2 h-5 w-5 transform group-hover:text-gray-400 transition-colors ease-in-out duration-150" viewBox="0 0 20 20" aria-hidden="true">
                <path d="M6 6L14 10L6 14V6Z" fill="currentColor" />
              </svg>
              Exercise 5
            </button>
            {openMenuItems.has(5) && (
              <div className="space-y-1" id="sub-menu-4">
                <Link to="/5/1" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  BrowserRouter
                </Link>
                <Link to="/5/2" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  useHistory
                </Link>
              </div>
            )}
          </div>
          <div className="space-y-1">
            {/* Current: "bg-gray-100 text-gray-900", Default: "bg-white text-gray-600 hover:bg-gray-50 hover:text-gray-900" */}
            <button
              type="button"
              className="bg-white text-gray-600 hover:bg-gray-50 hover:text-gray-900 group w-full flex items-center pr-2 py-2 text-sm font-medium rounded-md focus:outline-none focus:ring-2 focus:ring-indigo-500"
              aria-controls="sub-menu-4"
              aria-expanded="false"
              onClick={() => toggleMenuItem(6)}
            >
              {/* Expanded: "text-gray-400 rotate-90", Collapsed: "text-gray-300" */}
              <svg className="text-gray-300 mr-2 h-5 w-5 transform group-hover:text-gray-400 transition-colors ease-in-out duration-150" viewBox="0 0 20 20" aria-hidden="true">
                <path d="M6 6L14 10L6 14V6Z" fill="currentColor" />
              </svg>
              Exercise 6
            </button>
            {openMenuItems.has(6) && (
              <div className="space-y-1" id="sub-menu-4">
                <Link to="/6/1" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  Error
                </Link>
                <Link to="/6/2" className="group w-full flex items-center pl-10 pr-2 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                  Error handling
                </Link>
              </div>
            )}
          </div>
        </nav>
      </div>
    </div>
  )
};

export default Navigation;
