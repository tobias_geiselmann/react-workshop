import { ComposingComponents } from 'exercises/02-jsx-and-components/02-composing-components';
import { StyledWelcome } from 'exercises/02-jsx-and-components/03-style-property';
import { ReactFragment } from 'exercises/02-jsx-and-components/04-react-fragment';
import { PropTypesExample } from 'exercises/02-jsx-and-components/05-prop-types';
import { User } from 'exercises/04-data-fetching/01-useEffect';
import { CallbackUser } from 'exercises/04-data-fetching/03-useCallback'
import { Bug } from 'exercises/06-error-handling/01-error';
import { BugWithErrorBoundary } from 'exercises/06-error-handling/02-error-boundary';
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import Navigation from './components/navigation';
import { SimpleFunctionComponent } from './exercises/02-jsx-and-components/01-simple-function-component';
import { Counter } from './exercises/03-state-management/01-counter';
import { ControlledInput } from './exercises/03-state-management/02-controlled-input';
import { ComposedCounter } from './exercises/03-state-management/03-lifting-state-up';
import { ComposedContextCounter } from './exercises/03-state-management/04-use-context';
import { BlinkyRender, NonBlinkyRender } from './exercises/04-data-fetching/02-useLayoutEffect';
import { RouterDemo } from './exercises/05-browser-routing/01-router'
import { HistoryDemo } from './exercises/05-browser-routing/02-useHistory'

const App = () => {
  return (
    <Router>
      <div className="h-screen flex overflow-hidden bg-white">
        {/* Static sidebar for desktop */}
        <div className="hidden md:flex md:flex-shrink-0">
          <div className="flex flex-col w-64">
            {/* Sidebar component, swap this element with another sidebar if you like */}
            <Navigation />
          </div>
        </div>
        <div className="flex flex-col w-0 flex-1 overflow-hidden">
          <div className="md:hidden pl-1 pt-1 sm:pl-3 sm:pt-3">
            <button className="-ml-0.5 -mt-0.5 h-12 w-12 inline-flex items-center justify-center rounded-md text-gray-500 hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
              <span className="sr-only">Open sidebar</span>
              {/* Heroicon name: outline/menu */}
              <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16M4 18h16" />
              </svg>
            </button>
          </div>
          <main className="flex-1 relative z-0 overflow-y-auto focus:outline-none" tabIndex={0}>
            <div className="py-6 h-full">
              <div className="h-full mx-auto px-4 sm:px-6 md:px-8 prose prose-lg flex flex-col items-center justify-center max-w-sm">
                {/* Replace with your content */}
                <Switch>
                  <Route path="/2/1">
                    <SimpleFunctionComponent />
                  </Route>
                  <Route path="/2/2">
                    <ComposingComponents />
                  </Route>
                  <Route path="/2/3">
                    <StyledWelcome />
                  </Route>
                  <Route path="/2/4">
                    <ReactFragment />
                  </Route>
                  <Route path="/2/5">
                    <PropTypesExample />
                  </Route>
                  <Route path="/3/1">
                    <Counter />
                  </Route>
                  <Route path="/3/2">
                    <ControlledInput />
                  </Route>
                  <Route path="/3/3">
                    <ComposedCounter />
                  </Route>
                  <Route path="/3/4">
                    <ComposedContextCounter />
                  </Route>
                  <Route path="/4/1">
                    <User />
                  </Route>
                  <Route path="/4/2">
                    <CallbackUser />
                  </Route>
                  <Route path="/4/3">
                    <BlinkyRender />
                    <NonBlinkyRender />
                  </Route>
                  <Route path="/5/1">
                    <RouterDemo />
                  </Route>
                  <Route path="/5/2">
                    <HistoryDemo />
                  </Route>
                  <Route path="/6/1">
                    <Bug />
                  </Route>
                  <Route path="/6/2">
                    <BugWithErrorBoundary />
                  </Route>
                  <Route>
                    <h2 className="text-center">Welcome to the React Workshop App</h2>
                    <p className="text-center text-gray-600">Use the navigation bar on the left to switch between exercises.</p>
                  </Route>
                </Switch>
                {/* /End replace */}
              </div>
            </div>
          </main>
        </div>
      </div>
    </Router>
  )
};

export default App;
